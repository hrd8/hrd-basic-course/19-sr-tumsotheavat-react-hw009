
import {useEffect, useState} from 'react';
import {ToggleButton, ToggleButtonGroup} from "react-bootstrap";
import {Route,Switch,useHistory} from "react-router-dom";
import Movie from "../component/Movie";
import Animation from "../component/Animation";
const Video = ({match}) => {
    const [isMovie,setIsMovie] = useState(true);
    const [selectedValue,setSelectedValue] = useState({
        isAnimation: false,
        value : "Adventure"
    });
    const history = useHistory();
    useEffect(()=>{
        selectedValue.isAnimation ? history.push(`${match.path}/animation?name=${selectedValue.value}`) : history.push(`${match.path}/movie?name=${selectedValue.value}`)
    },[history, match.path, selectedValue])
    useEffect(()=>{
        isMovie ? history.push(`${match.path}/movie`) : history.push(`${match.path}/animation`)
    },[history, isMovie, match.path])

    return (
        <>
            <h1 className="my-1 rubik">Video</h1>
            <ToggleButtonGroup type="radio" name="options" defaultValue={1} className="my-3">
                <ToggleButton id="showInTable" value={1} variant="secondary" onClick={() => setIsMovie(true)} className="rubik">
                    Movie
                </ToggleButton>
                <ToggleButton id="showInCard" value={2} variant="secondary" onClick={() => setIsMovie(false)} className="rubik">
                    Animation
                </ToggleButton>
            </ToggleButtonGroup>
            <div>
                <Switch>
                    <Route path={`${match.path}/movie`} >
                        <Movie setSelectedValue={setSelectedValue}/>
                    </Route>
                    <Route path={`${match.path}/animation`} >
                        <Animation setSelectedValue={setSelectedValue}/>
                    </Route>
                </Switch>
            </div>
        </>
    );
};

export default Video;
