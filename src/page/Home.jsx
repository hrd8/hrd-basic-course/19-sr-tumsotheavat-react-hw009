import React from 'react';
import {Button, Card, Col, Row} from "react-bootstrap";
import {useHistory} from "react-router-dom";
const Home = ({data}) => {
    const history = useHistory();
    return (
        <div className="custom-container elevatedMargin">
            <Row>
                {data.map((item,index) => (
                    <Col lg={3} sm={6} xs={12} key={index}>
                        <Card style={{ width: '18rem' }} className="cardDefaultMinHeight cardContentAlignment">
                            <Card.Img variant="top" src={item.image} />
                            <Card.Body>
                                <Card.Title>{item.name}</Card.Title>
                                <Card.Text>
                                    {item.color}
                                </Card.Text>
                                <Button variant="primary" align="center" onClick={()=>history.push("/DisplayCardById/"+index)}>Read</Button>
                            </Card.Body>
                        </Card>
                    </Col>
                    ))}
            </Row>

        </div>
    );
};

export default Home;
