import React from 'react';
import {Button} from "react-bootstrap";

const Welcome = ({setIsLoggedIn}) => {
    return (
        <div>
            <h1 className="rubik elevatedMargin">Congratulation you logged in!</h1>
            <Button variant="danger" onClick={()=>setIsLoggedIn(false)}> log out</Button>
        </div>
    );
};

export default Welcome;