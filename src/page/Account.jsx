import React, {useEffect, useState} from 'react';
import {Switch,Route,useHistory} from "react-router-dom"
import AccountMenu from "../component/AccountMenu";
const Account = ({match}) => {
    const [selectedAccount,setSelectedAccount] = useState("Netflix");
    const history = useHistory();
    useEffect(()=>{
        history.push(`${match.path}/${selectedAccount}`)
    },[history, match.path, selectedAccount])
    return (
        <div>
            <Switch>
                <Route path={`${match.path}/:name`} >
                    <AccountMenu setSelectedValue={setSelectedAccount}/>
                </Route>
            </Switch>
        </div>
    );
};

export default Account;
