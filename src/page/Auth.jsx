import React from 'react';
import {Button} from "react-bootstrap";
import {useHistory} from "react-router-dom";

const Auth = ({login:{setIsLoggedIn, isLoggedIn}}) => {
    const history = useHistory();
    return (
        <div>
            {
                isLoggedIn ? (
                        <div>
                            <h1 className="rubik elevatedMargin">You already logged in!</h1>
                            <Button variant="danger" onClick={()=>setIsLoggedIn(false)}> log out</Button>
                        </div>
                    )
                    :
                    (
                        <div>
                            <h1 className="rubik elevatedMargin">Please Sign In Fist to Access Welcome Page!</h1>
                            <Button variant="success" onClick={()=>{
                                setIsLoggedIn(true)
                                history.push('/welcome');
                                }
                            } style={{marginRight:"10px"}}> sign In</Button>
                            <Button variant="danger" onClick={()=>history.push('/')}> Home</Button>
                        </div>

                    )
            }
        </div>
    );
};

export default Auth;
