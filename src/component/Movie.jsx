import React from 'react';
import {ToggleButton, ToggleButtonGroup} from "react-bootstrap";
import {useLocation} from "react-router-dom"
import queryString from "query-string"
const Movie = ({setSelectedValue}) => {
    const location = useLocation();
    const category = queryString.parse(location.search);
    let handleSelectCategory=(event)=>{
        setSelectedValue({
            isAnimation: false,
            value : event.target.value
        });
    }
    return (
        <div>
            <h1 className="my-1 rubik">Movie Category</h1>
            <ToggleButtonGroup type="radio" name="options" defaultValue={"Adventure"} className="my-3">
                <ToggleButton id="showInTable" value={"Adventure"} variant="secondary"  className="rubik" onClick={(event)=>handleSelectCategory(event)}>
                    Adventure
                </ToggleButton>
                <ToggleButton id="showInTable" value={"Crime"} variant="secondary"  className="rubik" onClick={(event)=>handleSelectCategory(event)}>
                    Crime
                </ToggleButton>
                <ToggleButton id="showInTable" value={"Romance"} variant="secondary"  className="rubik" onClick={(event)=>handleSelectCategory(event)}>
                    Romance
                </ToggleButton>
                <ToggleButton id="showInTable" value={"Comedy"} variant="secondary"  className="rubik" onClick={(event)=>handleSelectCategory(event)}>
                    Comedy
                </ToggleButton>
                <ToggleButton id="showInCard" value={"Action"} variant="secondary"  className="rubik" onClick={(event)=>handleSelectCategory(event)}>
                    Action
                </ToggleButton>
            </ToggleButtonGroup>
            <h1 className="rubik">Chosen Category : {category.name}</h1>
        </div>
    );
};

export default Movie;
