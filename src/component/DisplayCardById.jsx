import React from 'react';
import {useHistory, useParams} from 'react-router-dom'
import {Button, Card} from "react-bootstrap";
const DisplayCardById = ({data}) => {
    const param = useParams();
    const history = useHistory()
    return (
        <div className="d-flex justify-content-center elevatedMargin">
            <Card style={{ width: '18rem' }} className="cardDefaultMinHeight cardContentAlignment cardDisplayWidth">
                <Card.Img variant="top" src={data[param.id].image} />
                <Card.Body>
                    <Card.Title>{data[param.id].name} ({param.id})</Card.Title>
                    <Card.Text>
                        {data[param.id].color}
                    </Card.Text>
                    <Card.Text>
                        {data[param.id].description}
                    </Card.Text>
                    <Button variant="danger" align="center" onClick={()=>history.goBack()}>Back</Button>
                </Card.Body>
            </Card>
        </div>
    );
};

export default DisplayCardById;
