import React from 'react';
import {ToggleButton, ToggleButtonGroup} from "react-bootstrap";
import {useLocation} from "react-router-dom"
import queryString from "query-string"
const Animation = ({setSelectedValue}) => {
    const location = useLocation();
    const category = queryString.parse(location.search);
    let handleSelectCategory=(event)=>{
        setSelectedValue({
            isAnimation: true,
            value : event.target.value
        });
    }
    return (
        <div>
            <h1 className="my-1 rubik">Animation Category</h1>
            <ToggleButtonGroup type="radio" name="options" defaultValue={"Romance"} className="my-3">
                <ToggleButton id="showInTable" value={"Romance"} variant="secondary"  className="rubik" onClick={(event)=>handleSelectCategory(event)}>
                    Romance
                </ToggleButton>
                <ToggleButton id="showInTable" value={"Comedy"} variant="secondary"  className="rubik" onClick={(event)=>handleSelectCategory(event)}>
                    Comedy
                </ToggleButton>
                <ToggleButton id="showInCard" value={"Action"} variant="secondary"  className="rubik" onClick={(event)=>handleSelectCategory(event)}>
                    Action
                </ToggleButton>
            </ToggleButtonGroup>
            <h1 className="rubik">Chosen Category : {category.name}</h1>
        </div>
    );
};

export default Animation;
