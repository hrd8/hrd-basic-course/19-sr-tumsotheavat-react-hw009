import React from 'react';
import {ToggleButton, ToggleButtonGroup} from "react-bootstrap";
import {useParams} from "react-router-dom"
const AccountMenu = ({setSelectedValue}) => {
    const param = useParams()
    let handleSelectCategory =(event)=>{
        setSelectedValue(event.target.value)
    }
    return (
        <div>
            <h1 className="my-1 rubik">Account List</h1>
            <ToggleButtonGroup type="radio" name="options" defaultValue={"Netflix"} className="my-3">
                <ToggleButton id="showInTable" value={"Netflix"} variant="secondary"  className="rubik" onClick={(event)=>handleSelectCategory(event)}>
                    Netflix
                </ToggleButton>
                <ToggleButton id="showInTable" value={"Facebook"} variant="secondary"  className="rubik" onClick={(event)=>handleSelectCategory(event)}>
                    Facebook
                </ToggleButton>
                <ToggleButton id="showInTable" value={"Gmail"} variant="secondary"  className="rubik" onClick={(event)=>handleSelectCategory(event)}>
                    Gmail
                </ToggleButton>
                <ToggleButton id="showInTable" value={"Github"} variant="secondary"  className="rubik" onClick={(event)=>handleSelectCategory(event)}>
                    Github
                </ToggleButton>
            </ToggleButtonGroup>
            <h1 className="rubik">Chosen Account : {param.name}</h1>
        </div>
    );
};

export default AccountMenu;
